# RDF Scripts
This repository contains *one-off* Python scripts for TERN's ecological data integration work. The scripts are kept here for preservation more than reuse as most of the scripts are highly domain specific. 

## PoolParty Authentication Example
Example on authenticating as a user to PoolParty.

* [pp_auth/__init__.py](pp_auth/__init__.py)

## Replacing existing URIs with new URIs with the last segment as generated UUID

* [convert_uris_to_uuid](convert_uris_to_uuid)

## Convert AusPlots Yellow Book Look Up Tables to SKOS Vocabularies
Applicable AusPlots Rangelands' look up tables sourced originally from the Yellow Book are processed with Python scripts to convert the tabular data into SKOS-encoded vocabularies. 

* [convert_ausplots_table_to_skos](convert_ausplots_table_to_skos)

# Contact
**Edmond Chuc**  
*Software Engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  

