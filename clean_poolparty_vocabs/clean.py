# Author:   Edmond Chuc
# Purpose:  Read in a file 'test.ttl' and clean it by removing properties that are not from SKOS. Some exceptions
#           include some properties form DCTERMS and RDFS.
# Output:   output.ttl

from rdflib import Graph, Namespace, Literal
from rdflib.namespace import DCTERMS, RDFS, XSD, RDF

SKOS = Namespace('http://www.w3.org/2004/02/skos/core#')

g = Graph()
g.parse('test.ttl', format='turtle')

revised_g = Graph()
revised_g.bind('skos', SKOS)
revised_g.bind('dct', DCTERMS)

for s, p, o in g.triples((None, None, None)):
    if str(p).startswith('http://www.w3.org/2004/02/skos/core') or str(o).startswith('http://www.w3.org/2004/02/skos/core') or p == DCTERMS.source or p == DCTERMS.created or p == DCTERMS.modified or p == DCTERMS.description or p == DCTERMS.title or p == RDFS.label:
        if str(p).startswith('http://semantic-web.at/ontologies/csw.owl#'):
            continue
        if type(o) == Literal:
            if not str(p) == str(DCTERMS.title):
                if o.datatype == XSD.dateTime:
                    revised_g.add((s, p, o))
                else:
                    revised_g.add((s, p, Literal(str(o))))
        else:
            if not str(p) == "http://www.w3.org/2004/02/skos/core#inScheme" and not str(p) == "http://www.w3.org/2004/02/skos/core#topConceptOf" and not str(p) == 'http://www.w3.org/2004/02/skos/core#hasTopConcept' and not str(o) == str(SKOS.ConceptScheme):
                revised_g.add((s, p, o))

        if p == SKOS.prefLabel or p == DCTERMS.title:
            revised_g.add((s, RDFS.label, Literal(str(o))))
            revised_g.add((s, SKOS.prefLabel, Literal(str(o))))

        if p == SKOS.hasTopConcept:
            revised_g.add((s, SKOS.member, o))

        if o == SKOS.ConceptScheme:
            revised_g.add((s, RDF.type, SKOS.Collection))

revised_g.serialize('output.ttl', format='turtle')
