from rdflib import Graph
from rdflib.namespace import RDF, SKOS

g = Graph().parse('input.ttl', format='turtle')

for s, p, o in g.triples((None, RDF.type, SKOS.Concept)):
    print(str(s))