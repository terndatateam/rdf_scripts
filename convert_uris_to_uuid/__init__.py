# Author:   Edmond Chuc

# Purpose:  Convert existing URIs of SKOS concept schemes and concepts into a URI that's made up of a base URI and a
#           UUID.


from rdflib import Graph, URIRef
from rdflib.namespace import RDF, SKOS, DCTERMS, RDFS

from uuid import uuid4


def get_label(uri, g):
    for label in g.objects(URIRef(uri), SKOS.prefLabel):
        return label.title()
    for label in g.objects(URIRef(uri), DCTERMS.title):
        return label.title()
    for label in g.objects(URIRef(uri), RDFS.label):
        return label.title()

    raise Exception("No label found for {}".format(uri))


if __name__ == '__main__':

    BASE_URI = 'http://linked.data.gov.au/def/corveg-cv/'

    g = Graph().parse('input.ttl', format='turtle')

    with open('input.ttl', 'r') as f:
        input_data = f.read()
        # print(input_data)

    report_concept_schemes = []

    for s, p, o in g.triples((None, RDF.type, SKOS.ConceptScheme)):
        uuid = str(uuid4())
        new_uri = BASE_URI + uuid
        label = get_label(s, g)
        print(label, str(s), '->', new_uri)
        input_data = input_data.replace(f'<{str(s)}>', f'<{new_uri}>')
        report_concept_schemes.append((label, str(s), new_uri))

    with open('report_concept_scheme.csv', 'w') as f:
        # Write CSV header
        f.write('prefLabel,former_uri,new_uri\n')

        for line in report_concept_schemes:
            f.write(f'{line[0]}, {line[1]}, {line[2]}\n')

    report_concepts = []

    for s, p, o in g.triples((None, RDF.type, SKOS.Concept)):
        uuid = str(uuid4())
        new_uri = BASE_URI + uuid
        label = get_label(s, g)
        print(label, str(s), '->', new_uri)
        input_data = input_data.replace(f'<{str(s)}>', f'<{new_uri}>')
        report_concepts.append((label, str(s), new_uri))

    with open('report_concepts.csv', 'w') as f:
        # Write CSV header
        f.write('prefLabel,former_uri,new_uri\n')

        for line in report_concepts:
            f.write(f'{line[0]}, {line[1]}, {line[2]}\n')

    with open('output.ttl', 'w') as f:
        f.write(input_data)