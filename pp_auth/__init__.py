import requests


POOLPARTY_USERNAME = ''
POOLPARTY_PASSWORD = ''

POOLPARTY_PROJECT_ID = '1E148DAD-D6B8-0001-384E-1DAF10CAC1B0'
PP_SPARQL_ENDPOINT = 'https://editor.vocabs.ands.org.au/PoolParty/sparql/TERNQueenslandCORVEGdatabasevocabularies'

session = requests.Session()
session.auth = (POOLPARTY_USERNAME, POOLPARTY_PASSWORD)
auth = session.post(PP_SPARQL_ENDPOINT + '?query=PREFIX%20dcterms%3A%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0APREFIX%20skos%3A%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0Aselect%20*%0Awhere%20%7B%0A%20%20%20%20%3Fs%20dcterms%3Acreated%20%3Fdate%20.%0A%7D%0A')

print(auth.content.decode('utf-8'))
