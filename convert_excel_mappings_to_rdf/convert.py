# Author:   Edmond Chuc
#
# Purpose:  Convert each row in an Excel file to an rdf:Statement.
#
# Input:    Excel file
#           Columns:
#               subject | predicate | object | created | creator | description
#
# Output:   output.ttl

import pandas as pd

from rdflib import Literal, URIRef, Graph, Namespace
from rdflib.namespace import XSD, RDF, DCTERMS

from uuid import uuid4


def get_base_uri(uri : URIRef):
    local_uri = uri.split('#')[-1].split('/')[-1]
    uri = uri.replace(local_uri, '')
    return Namespace(uri)


if __name__ == '__main__':
    df = pd.read_excel('mappings.xlsx')

    g = Graph()
    g.bind('dcterms', DCTERMS)

    for i, row in df.iterrows():
        s = URIRef(row['subject'])
        p = URIRef(row['predicate'])
        o = URIRef(row['object'])
        created = Literal(row['created'])
        creator = URIRef(row['creator'])
        description = Literal(row['description'])

        NS = get_base_uri(s)
        local = str(uuid4())

        g.add((NS[local], RDF.type, RDF.Statement))
        g.add((NS[local], RDF.subject, s))
        g.add((NS[local], RDF.predicate, p))
        g.add((NS[local], RDF.object, o))
        g.add((NS[local], DCTERMS.created, created))
        g.add((NS[local], DCTERMS.creator, creator))
        g.add((NS[local], DCTERMS.description, description))

    g.serialize('output.ttl', format='turtle')