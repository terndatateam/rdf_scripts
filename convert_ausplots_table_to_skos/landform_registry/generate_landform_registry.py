from rdflib import Graph, RDF, Literal, URIRef, Namespace
from tern_rdf import TernRdf, RDFS, DCTERMS, REG, SKOS, UI

import datetime

g = Graph()
g.bind('rdfs', TernRdf.prefixes['rdfs'])
g.bind('reg', TernRdf.prefixes['reg'])
g.bind('skos', TernRdf.prefixes['skos'])
g.bind('dct', TernRdf.prefixes['dct'])
g.bind('owl', TernRdf.prefixes['owl'])
g.bind('ui', TernRdf.prefixes['ui'])

NS = Namespace('landform')
PID_NS = Namespace('http://anzsoil.org/def/au/asls/landform/')

# Registry
# g.add((URIRef(str(NS)), RDF.type, LDP.Container))
g.add((URIRef(str(NS)), RDF.type, REG.Register))
g.add((URIRef(str(NS)), RDFS.label, Literal('Landform classifiers', lang='en')))
g.add((URIRef(str(NS)), RDFS.seeAlso, URIRef('http://www.publish.csiro.au/nid/22/pid/5230.htm')))
g.add((URIRef(str(NS)), DCTERMS.creator, URIRef('https://orcid.org/0000-0002-6047-9864')))
g.add((URIRef(str(NS)), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-0693-1899')))
g.add((URIRef(str(NS)), DCTERMS.contributor, URIRef('https://orcid.org/0000-0002-3884-3420')))
g.add((URIRef(str(NS)), DCTERMS.description, Literal("""<p>This register contains a machine-readable representation of the classifiers described in chapter 5 <i>Landform</i>, by J.G. Speight, in <a href='https://www.publish.csiro.au/book/5230'>Australian soil and land survey field handbook (3rd edn)</a>.</p>
    <p>The description of landform in soil and land surveys has several purposes:</p>
    <ul>
        <li>it has direct application to land use planning</li>
        <li>the description is useful for finding relationships to support the extrapolation of point observations</li>
        <li>it helps to predict the land degradation that may follow various land uses.</li>
    </ul""", datatype=RDF.HTML)))
g.add((URIRef(str(NS)), DCTERMS.license, URIRef('http://creativecommons.org/licences/by/4.0')))
g.add((URIRef(str(NS)), DCTERMS.modified, Literal(datetime.datetime.now())))
g.add((URIRef(str(NS)), DCTERMS.publisher, URIRef('http://www.publish.csiro.au/')))
g.add((URIRef(str(NS)), DCTERMS.rights, Literal('copyright CSIRO 2009, 2016, 2018. All rights reserved.')))
g.add((URIRef(str(NS)), DCTERMS.source, Literal(
    "National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)")))
g.add((URIRef(str(NS)), REG.notation, Literal('landform')))
g.add((URIRef(str(NS)), UI.hierarchyChildProperty, SKOS.member))
g.add((URIRef(str(NS)), UI.hierarchyRootProperty, UI.topMemberOf))

g.serialize('landform-registry.ttl', format='turtle')