# Author:       Edmond Chuc
#
# Description:  Convert table into CSIRO Registry-compatible SKOS vocabulary for Landform Pattern Glossary
#               in the Yellow Book. All codes are exact matches to the Yellow Book except for the code 'NC'.

from rdflib import Literal, URIRef, Graph, Namespace
from rdflib.namespace import SKOS, RDF, DCTERMS, XSD, OWL
from tern_rdf import TernRdf, SOSA, RDFS, UI

import csv
import os

CSV_FILE = CSV_FILE = os.path.join(os.getcwd(), 'landform_pattern.csv')

# CSV Schema
ID = 'id'
LANDFORM_PATTERN = 'landform_pattern'
DESCRIPTION = 'description'
TYPICAL_ELEMENTS = 'typical_elements'
COMMON_ELEMENTS = 'common_elements'
OCCASIONAL_ELEMENTS = 'occasional_elements'
COMPARE_WITH = 'compare_with'

with open(CSV_FILE, 'r') as f:
    reader = csv.DictReader(f, delimiter=',')

    g = Graph()
    g.bind('owl', TernRdf.prefixes['owl'])
    g.bind('skos', TernRdf.prefixes['skos'])
    g.bind('ui', TernRdf.prefixes['ui'])
    g.bind('dct', TernRdf.prefixes['dct'])

    PID_NS = Namespace('http://anzsoil.org/def/au/asls/landform/')

    # skos:Collection, sosa:ObservableProperty
    collection_id = 'landform-pattern'
    # g.add((URIRef(str(NS)), RDFS.member, URIRef(collection_id)))
    g.add((URIRef(collection_id), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_id), RDF.type, SOSA.ObservableProperty))

    g.add((URIRef(collection_id), SKOS.prefLabel, Literal('Landform Pattern Glossary', lang='en')))
    g.add((URIRef(collection_id), DCTERMS.source, Literal(
        "National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)",
        lang='en')))
    g.add((URIRef(collection_id), UI.topMemberOf, URIRef('')))
    g.add((URIRef(collection_id), DCTERMS.identifier, Literal(PID_NS[collection_id], datatype=XSD.anyURI)))

    for row in reader:
        if row[ID] == 'NC':
            continue

        concept_id = collection_id + '-' + row[ID]

        # Add to register
        # g.add((URIRef(str(NS)), RDFS.member, URIRef(concept_id)))

        g.add((URIRef(concept_id), RDF.type, SKOS.Concept))
        g.add((URIRef(collection_id), SKOS.member, URIRef(concept_id)))

        g.add((URIRef(concept_id), SKOS.prefLabel, Literal(row[LANDFORM_PATTERN], lang='en')))
        g.add((URIRef(concept_id), RDFS.label, Literal(row[LANDFORM_PATTERN], lang='en')))
        g.add((URIRef(concept_id), SKOS.notation, Literal(row[ID], lang='en')))
        g.add((URIRef(concept_id), DCTERMS.identifier, Literal(PID_NS[concept_id], datatype=XSD.anyURI)))
        g.add((URIRef(concept_id), OWL.sameAs, PID_NS[concept_id]))
        g.add((URIRef(concept_id), UI.isMemberOf, URIRef(collection_id)))

        definition = '{}'.format(row[DESCRIPTION].replace(';', ','))
        definition = definition[0].lower() + definition[1:]
        if definition[-1] != '.':
            definition += '.'

        NONE_SPECIFIED = 'None specified'

        if row[TYPICAL_ELEMENTS] != NONE_SPECIFIED:
            definition += '\nTypical elements: {}.'.format(
                row[TYPICAL_ELEMENTS][0].lower() + row[TYPICAL_ELEMENTS][1:].replace(';', ','))

        if row[COMMON_ELEMENTS] != NONE_SPECIFIED:
            definition += '\nCommon elements: {}.'.format(
                row[COMMON_ELEMENTS][0].lower() + row[COMMON_ELEMENTS][1:].replace(';', ','))

        if row[OCCASIONAL_ELEMENTS] != NONE_SPECIFIED:
            definition += '\nOccasional elements: {}.'.format(
                row[OCCASIONAL_ELEMENTS][0].lower() + row[OCCASIONAL_ELEMENTS][1:].replace(';', ','))

        if row[COMPARE_WITH] != NONE_SPECIFIED:
            definition += '\nCompare with {}.'.format(row[COMPARE_WITH].replace(';', ','))

        g.add((URIRef(concept_id), SKOS.definition, Literal(definition, lang='en')))
        g.add((URIRef(concept_id), DCTERMS.description, Literal(definition, lang='en')))

    g.serialize('landform_pattern.ttl', format='turtle')
