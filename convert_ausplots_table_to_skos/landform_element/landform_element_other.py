# Author: Edmond Chuc
#
# Description: Convert the 'other' values of the look-up table for Landform Elements to a SKOS-encoded vocabulary.

from rdflib import Graph, Namespace, Literal, URIRef
from rdflib.namespace import RDF, SKOS

import csv
import os
from uuid import uuid4

CSV_FILE = os.path.join(os.getcwd(), 'landform_element_other.csv')
OUTPUT_FILE = os.path.join(os.getcwd(), 'landform_element_other.ttl')

# CSV Schema
CODE = 'code'
LANDFORM_ELEMENT = 'landform_element'
DESCRIPTION = 'description'

with open(CSV_FILE, 'r') as f:
    reader = csv.DictReader(f, delimiter=',')

    g = Graph()

    NS = Namespace('http://linked.data.gov.au/def/ausplots-cv/')

    top_concept_id = str(uuid4())

    g.add((NS[top_concept_id], RDF.type, SKOS.Concept))
    g.add((NS[top_concept_id], SKOS.prefLabel, Literal('Landform Element', lang='en')))
    g.add((NS[top_concept_id], SKOS.definition, Literal('AusPlots Rangelands uses the codes and definitions for landform element from the ASLS (Yellow Book), chapter 5. This concept scheme contains the additional terms found in the look-up table of "lut_landform_element" that do not belong in the same section as the other codes of the Australian Soil and Land Survey', lang='en')))
    g.add((NS[top_concept_id], SKOS.topConceptOf,
           URIRef('http://linked.data.gov.au/def/ausplots-cv/134f00d9-8eb8-4e15-aaa4-4db102bc3d2f')))

    for row in reader:
        concept_id = str(uuid4())

        # g.add((NS[concept_scheme_id], SKOS.hasTopConcept, NS[concept_id]))
        # g.add((NS[concept_id], SKOS.topConceptOf, NS[concept_scheme_id]))
        g.add((NS[concept_id], SKOS.broader, NS[top_concept_id]))
        g.add((NS[top_concept_id], SKOS.narrower, NS[concept_id]))

        g.add((NS[concept_id], RDF.type, SKOS.Concept))
        g.add((NS[concept_id], SKOS.prefLabel, Literal(row[LANDFORM_ELEMENT], lang='en')))
        g.add((NS[concept_id], SKOS.notation, Literal(row[CODE], lang='en')))
        g.add((NS[concept_id], SKOS.definition, Literal(row[CODE], lang='en')))

    g.serialize(OUTPUT_FILE, format='turtle')
