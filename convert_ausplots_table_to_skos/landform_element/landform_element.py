# Author: Edmond Chuc
#
# Description:  Convert the table into CSIRO Registry-compatible SKOS vocabulary for Landform Element Key and Glossary
#               in the Yellow Book. All codes match except for the ones in the 'description' column that are *null* and
#               the code where its value is 'NC'.

from rdflib import Literal, URIRef, Graph, Namespace
from rdflib.namespace import SKOS, RDF, DCTERMS, XSD, OWL
from tern_rdf import TernRdf, SOSA, RDFS, UI

import csv
import os

CSV_FILE = os.path.join(os.getcwd(), 'landform_element.csv')

# CSV schema
CODE = 'code'
LANDFORM_ELEMENT = 'landform_element'
DESCRIPTION = 'description'

with open(CSV_FILE, 'r') as f:
    reader = csv.DictReader(f, delimiter=',')

    g = Graph()
    g.bind('owl', TernRdf.prefixes['owl'])
    g.bind('skos', TernRdf.prefixes['skos'])
    g.bind('ui', TernRdf.prefixes['ui'])
    g.bind('dct', TernRdf.prefixes['dct'])

    PID_NS = Namespace('http://anzsoil.org/def/au/asls/landform/')

    collection_id = 'landform-element'

    g.add((URIRef(collection_id), RDF.type, SKOS.Collection))
    g.add((URIRef(collection_id), RDF.type, SOSA.ObservableProperty))
    g.add((URIRef(collection_id), SKOS.prefLabel, Literal('Landform Element Key and Glossary', lang='en')))
    g.add((URIRef(collection_id), DCTERMS.source, Literal(
        "National Committee on Soil and Terrain (2009), 'Australian soil and land survey field handbook (3rd edn).' (CSIRO Publishing: Melbourne)",
        lang='en')))
    g.add((URIRef(collection_id), UI.topMemberOf, URIRef('')))
    g.add((URIRef(collection_id), DCTERMS.identifier, Literal(PID_NS[collection_id], datatype=XSD.anyURI)))

    for row in reader:
        if row[CODE] == 'NC':
            continue
        if row[DESCRIPTION] == '':
            continue

        concept_id = collection_id + '-' + row[CODE]

        g.add((URIRef(concept_id), RDF.type, SKOS.Concept))
        g.add((URIRef(collection_id), SKOS.member, URIRef(concept_id)))

        g.add((URIRef(concept_id), SKOS.prefLabel, Literal(row[LANDFORM_ELEMENT], lang='en')))
        g.add((URIRef(concept_id), RDFS.label, Literal(row[LANDFORM_ELEMENT], lang='en')))
        g.add((URIRef(concept_id), SKOS.notation, Literal(row[CODE], lang='en')))
        g.add((URIRef(concept_id), DCTERMS.identifier, Literal(PID_NS[concept_id], datatype=XSD.anyURI)))
        g.add((URIRef(concept_id), OWL.sameAs, PID_NS[concept_id]))
        g.add((URIRef(concept_id), UI.isMemberOf, URIRef(collection_id)))

        definition = '{}'.format(row[DESCRIPTION].replace(';', ','))
        definition = definition[0].lower() + definition[1:]
        if definition[-1] != '.':
            definition += '.'

        g.add((URIRef(concept_id), SKOS.definition, Literal(definition, lang='en')))
        g.add((URIRef(concept_id), DCTERMS.description, Literal(definition, lang='en')))

    g.serialize('landform_element.ttl', format='turtle')